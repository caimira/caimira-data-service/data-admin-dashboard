import { whoTheme } from "./styles"
import { AuthenticatedTemplate, UnauthenticatedTemplate } from "@azure/msal-react"
import { useApiCaller } from "./utils/api"

import { ThemeProvider } from "@mui/material/styles"
import { BrowserRouter as Router, Route, Routes } from "react-router-dom"

import LoginPage from "./routes/Login"

import "./App.css"
import Authenticated from "./routes/Authenticated"
import React from "react"

let theme = whoTheme()

theme = {
  ...theme,
  components: {
    MuiDrawer: {
      styleOverrides: {
        paper: {
          backgroundColor: "#081627"
        }
      }
    },
    MuiButton: {
      styleOverrides: {
        root: {
          textTransform: "none"
        },
        contained: {
          boxShadow: "none",
          "&:active": {
            boxShadow: "none"
          }
        }
      }
    },
    MuiTabs: {
      styleOverrides: {
        root: {
          marginLeft: theme.spacing(1)
        },
        indicator: {
          height: 3,
          borderTopLeftRadius: 3,
          borderTopRightRadius: 3,
          backgroundColor: theme.palette.common.white
        }
      }
    },
    MuiTab: {
      styleOverrides: {
        root: {
          textTransform: "none",
          margin: "0 16px",
          minWidth: 0,
          padding: 0,
          [theme.breakpoints.up("md")]: {
            padding: 0,
            minWidth: 0
          }
        }
      }
    },
    MuiIconButton: {
      styleOverrides: {
        root: {
          padding: theme.spacing(1)
        }
      }
    },
    MuiTooltip: {
      styleOverrides: {
        tooltip: {
          borderRadius: 4
        }
      }
    },
    MuiDivider: {
      styleOverrides: {
        root: {
          backgroundColor: "rgb(255,255,255,0.15)"
        }
      }
    },
    MuiListItemButton: {
      styleOverrides: {
        root: {
          "&.Mui-selected": {
            color: "#4fc3f7"
          }
        }
      }
    },
    MuiListItemText: {
      styleOverrides: {
        primary: {
          fontSize: 14,
          fontWeight: theme.typography.fontWeightMedium
        }
      }
    },
    MuiListItemIcon: {
      styleOverrides: {
        root: {
          color: "inherit",
          minWidth: "auto",
          marginRight: theme.spacing(2),
          "& svg": {
            fontSize: 20
          }
        }
      }
    },
    MuiAvatar: {
      styleOverrides: {
        root: {
          width: 32,
          height: 32
        }
      }
    }
  }
}

const App = () => {
  const apiCaller = useApiCaller()

  return (
    <ThemeProvider theme={theme}>
      <Router>
        <AuthenticatedTemplate>
          <Authenticated api={apiCaller} />
        </AuthenticatedTemplate>

        <UnauthenticatedTemplate>
          <div className="main">
            <Routes>
              <Route path="*" element={<LoginPage />} />
            </Routes>
          </div>
        </UnauthenticatedTemplate>
      </Router>
    </ThemeProvider>
  )
}

export default App
