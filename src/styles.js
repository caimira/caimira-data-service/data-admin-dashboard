import { createTheme } from "@mui/material/styles"

import { indigo } from "@mui/material/colors"

export const whoTheme = () => {
  let theme = createTheme({
    palette: {
      // type: "dark",
      /*
      primary: {
        light: "#4dabf5",
        main: "#2196f3",
        dark: "#1769aa",
      },
      */
      /*
       primary: {
         light: "#1D2856",
         main: "#1D2856",
         dark: "#1D2856",
       },
       secondary: {
         light: "#ECAB42",
         main: "#ECAB42",
         dark: "#ECAB42",
       },
       */
      primary: indigo,
      secondary: {
        main: "#283593"
      }
    },
    typography: {
      fontFamily: [
        "Roboto",
        "-apple-system",
        "BlinkMacSystemFont",
        '"Segoe UI"',
        '"Helvetica Neue"',
        "Arial",
        "sans-serif",
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"'
      ].join(","),
      h1: {
        fontFamily: [
          "Roboto",
          "-apple-system",
          "BlinkMacSystemFont",
          '"Segoe UI"',
          '"Helvetica Neue"',
          "Arial",
          "sans-serif",
          '"Apple Color Emoji"',
          '"Segoe UI Emoji"',
          '"Segoe UI Symbol"'
        ].join(",")
      },
      h2: {
        fontFamily: [
          "Roboto",
          "-apple-system",
          "BlinkMacSystemFont",
          '"Segoe UI"',
          '"Helvetica Neue"',
          "Arial",
          "sans-serif",
          '"Apple Color Emoji"',
          '"Segoe UI Emoji"',
          '"Segoe UI Symbol"'
        ].join(",")
      },
      h3: {
        fontFamily: [
          "Roboto",
          "-apple-system",
          "BlinkMacSystemFont",
          '"Segoe UI"',
          '"Helvetica Neue"',
          "Arial",
          "sans-serif",
          '"Apple Color Emoji"',
          '"Segoe UI Emoji"',
          '"Segoe UI Symbol"'
        ].join(","),
        fontSize: "2.7rem"
      },
      h5: {
        fontWeight: 500,
        fontSize: 26
      },
      h4: {
        fontWeight: 600
      }
    },
    shape: {
      borderRadius: 8
    },
    mixins: {
      toolbar: {
        minHeight: 48
      }
    }
  })

  theme = {
    ...theme,
    components: {
      MuiDrawer: {
        styleOverrides: {
          paper: {
            // backgroundColor: "#18202c",
          }
        }
      },
      MuiButton: {
        styleOverrides: {
          root: {
            textTransform: "none"
          },
          contained: {
            boxShadow: "none",
            "&:active": {
              boxShadow: "none"
            }
          }
        }
      },
      MuiTabs: {
        styleOverrides: {
          root: {
            marginLeft: theme.spacing(1)
          },
          indicator: {
            height: 3,
            borderTopLeftRadius: 3,
            borderTopRightRadius: 3,
            backgroundColor: theme.palette.common.white
          }
        }
      },
      MuiTab: {
        defaultProps: {
          disableRipple: true // No more ripple!
        },
        styleOverrides: {
          root: {
            textTransform: "none",
            margin: "0 16px",
            minWidth: 0,
            padding: 0,
            color: "white",
            [theme.breakpoints.up("md")]: {
              padding: 0,
              minWidth: 0
            }
          },
          selected: {
            color: "white"
          }
        }
      },
      MuiIconButton: {
        styleOverrides: {
          root: {
            padding: theme.spacing(1)
          }
        }
      },
      MuiTooltip: {
        styleOverrides: {
          tooltip: {
            borderRadius: 4
          }
        }
      },
      MuiDivider: {
        styleOverrides: {
          root: {
            backgroundColor: "#404854"
          }
        }
      },
      MuiListItemText: {
        styleOverrides: {
          primary: {
            fontWeight: theme.typography.fontWeightMedium
          }
        }
      },
      MuiListItemIcon: {
        styleOverrides: {
          root: {
            color: "inherit",
            marginRight: 0,
            "& svg": {
              fontSize: 20
            }
          }
        }
      },
      MuiAvatar: {
        styleOverrides: {
          root: {
            width: 32,
            height: 32
          }
        }
      }
    }
  }
  return theme
}
