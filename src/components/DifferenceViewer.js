import React from "react"
import PropTypes from "prop-types"

import Paper from "@mui/material/Paper"

import Table from "@mui/material/Table"
import TableBody from "@mui/material/TableBody"
import TableCell from "@mui/material/TableCell"
import TableContainer from "@mui/material/TableContainer"
import TableHead from "@mui/material/TableHead"
import TableRow from "@mui/material/TableRow"

function DifferenceViewer(props) {
  return (
    <TableContainer component={Paper}>
      <div className="difference_viewer">
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Type</TableCell>
              <TableCell>Key/Path</TableCell>
              <TableCell>Values</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {props.differences.removed.map((item, index) => (
              <TableRow key={index}>
                <TableCell className="removed">-</TableCell>
                <TableCell className="removed">{item.key}</TableCell>
                <TableCell className="removed">{JSON.stringify(item.value, null, 4)}</TableCell>
              </TableRow>
            ))}

            {props.differences.added.map((item, index) => (
              <TableRow key={index}>
                <TableCell className="added">+</TableCell>
                <TableCell className="added">{item.key}</TableCell>
                <TableCell className="added">{JSON.stringify(item.value, null, 4)}</TableCell>
              </TableRow>
            ))}

            {props.differences.updated.map((item, index) => (
              <TableRow key={index}>
                <TableCell></TableCell>
                <TableCell>{item.key}</TableCell>
                <TableCell>{`${JSON.stringify(item.old_value, null, 4)} -> ${JSON.stringify(item.new_value, null, 4)}`}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </div>
    </TableContainer>
  )
}

DifferenceViewer.propTypes = {
  differences: PropTypes.object.isRequired
}

export default DifferenceViewer
