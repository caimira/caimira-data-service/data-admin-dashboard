import React, { useLayoutEffect, useRef } from "react"
import * as d3 from "d3"

const PdfDiagram = ({ parameters }) => {
  const svgRef = useRef()

  useLayoutEffect(() => {
    // Check if all necessary parameters are provided
    if (!parameters || !parameters.frequencies || !parameters.log_variable || !parameters.kernel_bandwidth) {
      console.warn("Parameters or required fields are undefined.")
      return
    }

    // Ensure the SVG element is rendered
    if (!svgRef.current) {
      console.warn("SVG element is not yet rendered.")
      return
    }

    const data = {
      frequencies: parameters.frequencies,
      kernel_bandwidth: parameters.kernel_bandwidth,
      log_variable: parameters.log_variable
    }

    // Clear previous SVG content
    d3.select(svgRef.current).selectAll("*").remove()

    // Set up dimensions
    const width = 800
    const height = 400
    const margin = { top: 20, right: 30, bottom: 40, left: 50 }

    const svg = d3.select(svgRef.current).attr("width", width).attr("height", height)

    // Scales
    const xScale = d3
      .scaleLinear()
      .domain(d3.extent(data.log_variable))
      .range([margin.left, width - margin.right])

    const yScale = d3
      .scaleLinear()
      .domain([0, d3.max(data.frequencies)])
      .nice()
      .range([height - margin.bottom, margin.top])

    // Axes
    const xAxis = d3.axisBottom(xScale).ticks(10)
    const yAxis = d3.axisLeft(yScale).ticks(10)

    // Draw axes
    svg
      .append("g")
      .attr("transform", `translate(0, ${height - margin.bottom})`)
      .call(xAxis)
      .append("text")
      .attr("fill", "#000")
      .attr("x", width / 2)
      .attr("y", 35)
      .attr("text-anchor", "middle")
      .text("Log Variable")

    svg
      .append("g")
      .attr("transform", `translate(${margin.left}, 0)`)
      .call(yAxis)
      .append("text")
      .attr("fill", "#000")
      .attr("transform", "rotate(-90)")
      .attr("x", -height / 2)
      .attr("y", -40)
      .attr("text-anchor", "middle")
      .text("Frequency")

    // Line generator
    const line = d3
      .line()
      .x((d, i) => xScale(data.log_variable[i]))
      .y(d => yScale(d))

    // Draw line
    svg
      .append("path")
      .datum(data.frequencies)
      .attr("fill", "none")
      .attr("stroke", "steelblue")
      .attr("stroke-width", 2)
      .attr("d", line)
  }, [parameters])

  return (
    <div className="pdf-diagram">
      <svg ref={svgRef}></svg>
    </div>
  )
}

export default PdfDiagram
