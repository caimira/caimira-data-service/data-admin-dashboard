import * as React from "react"
import Accordion from "@mui/material/Accordion"
import AccordionSummary from "@mui/material/AccordionSummary"
import AccordionDetails from "@mui/material/AccordionDetails"
import ExpandMoreIcon from "@mui/icons-material/ExpandMore"
import { Typography } from "@mui/material"

export default function AdminDashboardAccordion({ title, id, children }) {
  const accordionTitleVariant =
    id === "caimira-data-service-admin-dashboard_virological_data_symptomatic_vl_frequencies" ? "body1" : "h5"

  return (
    <div key={id + "-accordion-component"} className="admin-dashboard-accordion">
      <Accordion variant="outlined">
        <AccordionSummary expandIcon={<ExpandMoreIcon />} aria-controls={id + "-content"} id={id + "-header"}>
          <Typography variant={accordionTitleVariant}>{title}</Typography>
        </AccordionSummary>
        <AccordionDetails>{children}</AccordionDetails>
      </Accordion>
    </div>
  )
}
