import * as React from "react"
import AppBar from "@mui/material/AppBar"
import Button from "@mui/material/Button"
import Grid from "@mui/material/Grid"
import Toolbar from "@mui/material/Toolbar"
import Typography from "@mui/material/Typography"
import { Code, OpenInNew, Schema } from "@mui/icons-material"
import { useNavigate } from "react-router-dom"

function Header() {
  const navigate = useNavigate()

  return (
    <React.Fragment>
      <AppBar
        color="primary"
        // position="sticky"
        position="relative"
        elevation={0}
      >
        <Toolbar>
          <Grid
            container
            display={"flex"}
            // justifyContent={"center"}
            alignItems={"center"}
          >
            <Grid item>
              <Button
                variant="contained"
                color="secondary"
                size="small"
                target="_blank"
                href="https://partnersplatform.who.int/aria"
                startIcon={<OpenInNew />}
              >
                WHO ARIA
              </Button>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
      <AppBar component="div" color="primary" position="static" elevation={0} sx={{ zIndex: 0, paddingBottom: 1 }}>
        <Toolbar>
          <Grid container alignItems="center" spacing={1}>
            <Grid item xs>
              <Typography color="inherit" variant="h4" component="h1">
                Parameter Database
              </Typography>
            </Grid>
          </Grid>
          <Grid minWidth={400} display={"flex"} justifyContent={"flex-end"} alignItems={"flex-end"}>
            <Button
              variant="contained"
              color="secondary"
              size="large"
              target="_blank"
              href="#schema"
              onClick={e => {
                e.preventDefault()
                navigate("schema")
              }}
              startIcon={<Schema />}
              style={{ marginRight: 8 }}
            >
              Schema / Editor
            </Button>
            <Button
              variant="contained"
              color="secondary"
              size="large"
              target="_blank"
              href="#jsoneditor"
              onClick={e => {
                e.preventDefault()
                navigate("json-editor")
              }}
              startIcon={<Code />}
            >
              JSON Editor
            </Button>
          </Grid>
        </Toolbar>
      </AppBar>
    </React.Fragment>
  )
}

Header.propTypes = {}

export default Header
