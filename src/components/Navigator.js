import React from "react"
import Divider from "@mui/material/Divider"
import Drawer from "@mui/material/Drawer"
import List from "@mui/material/List"
import Box from "@mui/material/Box"
import ListItem from "@mui/material/ListItem"
import ListItemButton from "@mui/material/ListItemButton"
import ListItemIcon from "@mui/material/ListItemIcon"
import ListItemText from "@mui/material/ListItemText"

import sections from "../data/sections"

import { Code, DataObject, IntegrationInstructions, OpenInNew, Restore } from "@mui/icons-material"

import { useNavigate } from "react-router-dom"

const categories = [
  {
    id: "Parameters",
    children: sections.map(section => {
      return {
        id: section.id,
        title: section.title,
        icon: <DataObject />,
        active: section.active,
        type: section.type
      }
    })
  }
]

const item = {
  py: "2px",
  px: 3,
  color: "rgba(255, 255, 255, 0.7)",
  "&:hover, &:focus": {
    bgcolor: "rgba(255, 255, 255, 0.08)"
  }
}

const itemCategory = {
  boxShadow: "0 -1px 0 rgb(255,255,255,0.1) inset",
  py: 1.5,
  px: 3
}

export default function Navigator(props) {
  const { ...other } = props

  const navigate = useNavigate()

  return (
    <Drawer variant="permanent" {...other}>
      <List disablePadding>
        <ListItem sx={{ ...item, ...itemCategory, fontSize: 22, color: "#fff" }}>
          <ListItemButton
            onClick={e => {
              e.preventDefault()
              navigate("/")
            }}
          >
            ARIA Data Service
          </ListItemButton>
        </ListItem>
        {false && (
          <ListItem sx={{ ...item, ...itemCategory }}>
            {true && (
              <ListItemIcon>
                <OpenInNew />
              </ListItemIcon>
            )}
            <ListItemButton href="https://partnersplatform.who.int/aria">WHO ARIA</ListItemButton>
          </ListItem>
        )}
        <ListItem sx={{ ...item, ...itemCategory }}>
          {true && (
            <ListItemIcon>
              <Restore />
            </ListItemIcon>
          )}
          <ListItemButton
            onClick={e => {
              e.preventDefault()
              navigate("history")
            }}
          >
            History
          </ListItemButton>
        </ListItem>
        {
          // eslint-disable-next-line no-unused-vars
          categories.map(({ id, title, children }) => (
            <Box key={id} sx={{ bgcolor: "#101F33" }}>
              <ListItem sx={{ py: 2, px: 3 }}>
                <ListItemText sx={{ color: "#fff" }}>{id}</ListItemText>
              </ListItem>
              {children.map(({ id: childId, title, icon, active, type }) => (
                <ListItem disablePadding key={childId}>
                  <ListItemButton
                    selected={active}
                    className={"nav-link" + (type === "subitem" ? " subitem" : "")}
                    sx={item}
                    // to={childId}
                    // component={Link}
                    onClick={e => {
                      e.preventDefault()
                      navigate(childId)
                    }}
                  >
                    {true && (
                      <ListItemIcon style={type === "subitem" ? { visibility: "hidden" } : {}}>{icon}</ListItemIcon>
                    )}
                    <ListItemText className={type === "title" ? "nav-link-text-title" : "nav-link-text-subitem"}>
                      {title}
                    </ListItemText>
                  </ListItemButton>
                </ListItem>
              ))}
              <Divider sx={{ mt: 2 }} />
            </Box>
          ))
        }
        <ListItem sx={{ ...item, ...itemCategory }}>
          {true && (
            <ListItemIcon>
              <IntegrationInstructions />
            </ListItemIcon>
          )}
          <ListItemButton
            onClick={e => {
              e.preventDefault()
              navigate("json")
            }}
          >
            Export JSON
          </ListItemButton>
        </ListItem>
        {false && (
          <ListItem sx={{ ...item, ...itemCategory }}>
            {true && (
              <ListItemIcon>
                <Code />
              </ListItemIcon>
            )}
            <ListItemButton
              onClick={e => {
                e.preventDefault()
                navigate("json-editor")
              }}
            >
              JSON Editor
            </ListItemButton>
          </ListItem>
        )}
      </List>
    </Drawer>
  )
}
