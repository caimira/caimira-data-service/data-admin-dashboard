import { Templates } from "@rjsf/mui"
import React from "react"

import PdfDiagram from "../diagrams/PdfDiagram"
import AdminDashboardAccordion from "../AdminDashboardAccordion"
import { Typography } from "@mui/material"

/**
 *
 * To enable the pdf diagram add this to the schema:
 * "ui:diagram": "pdf"
 *
 */

const PDF_DIAGRAM = "pdf"

const { FieldTemplate } = Templates

// reference: https://rjsf-team.github.io/react-jsonschema-form/docs/advanced-customization/custom-templates#fieldtemplate
function AdminDashboardFieldTemplate(props) {
  const { id, label, schema, formData } = props

  const diagram = "ui:diagram" in schema

  if (diagram) {
    const diagramType = schema["ui:diagram"]

    if (diagramType === PDF_DIAGRAM) {
      console.log("AdminDashboardFieldTemplate-PDF_DIAGRAM", formData, "PDF_DIAGRAM", PDF_DIAGRAM)

      const { parameters } = formData

      return (
        <>
          <Typography variant="h5">{label}</Typography>
          <PdfDiagram parameters={parameters} />
          <AdminDashboardAccordion id={id} title={"See all the values of " + label}>
            <FieldTemplate {...props} />
          </AdminDashboardAccordion>
        </>
      )
    }
  }

  return <FieldTemplate {...props} />
}

export default AdminDashboardFieldTemplate
