import React, { useEffect, useState } from "react"

import AdminDashboardAccordion from "../AdminDashboardAccordion"

import { TextField } from "@mui/material"
import { ADDITIONAL_PROPERTY_FLAG } from "@rjsf/utils"

// reference: https://rjsf-team.github.io/react-jsonschema-form/docs/advanced-customization/custom-templates/#wrapifadditionaltemplate

/**
 *
 * To enable the accordion add this to the schema:
 * "ui:accordion": true
 *
 */

// const { FieldTemplate } = Templates

function AdminDashboardWrapIfAdditionalTemplate(props) {
  const { id, label, schema, children, onKeyChange } = props
  const title = schema.title || label

  const accordion = "ui:accordion" in schema
  const customKey = "ui:customKey" in schema
  const additional = ADDITIONAL_PROPERTY_FLAG in schema

  const [keyLabel, setKeyLabel] = useState(label)

  useEffect(() => {
    setKeyLabel(label)
  }, [label])

  let wrappedChildren = Object.assign({}, children)

  if (customKey && additional) {
    wrappedChildren = (
      <React.Fragment key={`${id}-additional-key`}>
        {false && (
          <>
            <label label={"Text"} id={`${id}-key`}>
              Custom Field Key
            </label>
            <input
              className="form-control"
              type="text"
              id={`${id}-key`}
              onBlur={function (event) {
                onKeyChange(event.target.value)
              }}
              defaultValue={label}
            />
          </>
        )}
        <TextField
          key={`${id}-key-${keyLabel}`} // Ensure unique key for each TextField
          size="small"
          className="form-control admin-dashboard-custom-key"
          type="text"
          label={"Field Key"}
          id={`${id}-key-${keyLabel}`} // Ensure unique key for each TextField
          onBlur={function (event) {
            onKeyChange(event.target.value)
            setKeyLabel(event.target.value)
          }}
          defaultValue={keyLabel}
          variant="filled"
          fullWidth
        />
        {wrappedChildren}
      </React.Fragment>
    )
  }

  if (accordion) {
    wrappedChildren = (
      <AdminDashboardAccordion key={id + "-accordion"} id={id} title={additional ? label : title}>
        {wrappedChildren}
      </AdminDashboardAccordion>
    )
  }

  return wrappedChildren
}

export default AdminDashboardWrapIfAdditionalTemplate
