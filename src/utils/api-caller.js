import { trackPromise } from "react-promise-tracker"
export class ApiCaller {
  constructor(axiosInstance) {
    this.axiosInstance = axiosInstance

    this.formDataContentHeaders = {
      headers: {
        "Content-Type": "multipart/form-data"
      }
    }
  }

  getPing({ onSuccess, onFailure }) {
    return this.processAxiosCall(this.axiosInstance.get(`/ping`), onSuccess, onFailure)
  }

  getData({ onSuccess, onFailure }) {
    return this.processAxiosCall(this.axiosInstance.get(`/data`), onSuccess, onFailure)
  }

  getDataSchema({ onSuccess, onFailure }) {
    return this.processAxiosCall(this.axiosInstance.get(`/schema`), onSuccess, onFailure)
  }

  updateData({ onSuccess, onFailure, json_data, reason }) {
    return this.processAxiosCall(
      this.axiosInstance.put(`/data`, { json_data: json_data, reason: reason }),
      onSuccess,
      onFailure
    )
  }

  updateDataSchema({ onSuccess, onFailure, json_schema, json_data, reason }) {
    return this.processAxiosCall(
      this.axiosInstance.put(`/schema`, { json_schema, json_data: json_data, reason: reason }),
      onSuccess,
      onFailure
    )
  }

  getDataHistory({ onSuccess, onFailure, page_size, page_number }) {
    return this.processAxiosCall(
      this.axiosInstance.get(`/data/history/${page_size}/${page_number}`),
      onSuccess,
      onFailure
    )
  }

  //----------------------------------------------------------------------------
  // Utility methods
  //----------------------------------------------------------------------------
  processAxiosCall(promise, onSuccess, onFailure) {
    return trackPromise(
      promise
        .then(
          response => {
            const method = response.config.method.toUpperCase()
            const url = response.request.responseURL
            console.log(`${method} ${url} => ${response.status} (${response.statusText})`)
            onSuccess && onSuccess(response.data)
            return { data: response.data, error: null }
          },
          error => {
            console.log(error.code)
            const response = error.response
            console.log(error)
            const method = response.config.method.toUpperCase()
            const url = response.request.responseURL
            console.error(`${method} ${url} => ${response.status} (${response.statusText}): ${response.data}`)
            onFailure && onFailure(error)
            return { data: null, error }
          }
        )
        .catch(error => {
          onFailure && onFailure(error)
          console.error(error)
          return { data: null, error }
        })
    )
  }
}
