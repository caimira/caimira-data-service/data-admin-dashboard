import { ApiCaller } from "./api-caller"
import { loginRequest } from "../authConfig"

import axios from "axios"
import { useMsal } from "@azure/msal-react"
const caimira_api_url = process.env.REACT_APP_CAIMIRA_API_URL

const createAxiosInstance = () => {
  return axios.create({
    baseURL: `${caimira_api_url}`,
    withCredentials: true
  })
}

// Add a request interceptor

export const useApiCaller = () => {
  const { instance, accounts } = useMsal() // eslint-disable-line no-eval
  const axiosInstance = createAxiosInstance()
  axiosInstance.interceptors.request.use(
    async function (config) {
      // Get token header
      await instance
        .acquireTokenSilent({
          ...loginRequest,
          account: accounts[0]
        })
        .then(response => {
          config.headers["Authorization"] = `Bearer ${response.accessToken}`
        })
        .catch(async reason => {
          if (reason.name == "InteractionRequiredAuthError") {
            await instance.loginRedirect(loginRequest).catch(e => {
              console.log(e)
            })
          }
        })

      return config
    },
    function (error) {
      // Do something with request error
      return Promise.reject(error)
    }
  )
  return new ApiCaller(axiosInstance)
}
