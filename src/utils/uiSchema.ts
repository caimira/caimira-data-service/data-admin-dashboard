// eslint-disable-next-line no-unused-vars
import { UiSchema } from "@rjsf/utils"

export const uiSchema: UiSchema = {
  "ui:rootFieldId": "caimira-data-service-admin-dashboard",
  "ui:options": {
    classNames: "form-item",
    addable: true,
    removable: true
  }
}
