import React from "react"
import PropTypes from "prop-types"
import { useRef, useState } from "react"
import { Button, Paper, Typography } from "@mui/material"

import Form from "@rjsf/mui"
import validator from "@rjsf/validator-ajv8"

import { useParams } from "react-router-dom"

import sections from "../data/sections.json"

import AdminDashboardFieldTemplate from "../components/templates/AdminDashboardFieldTemplate"
import AdminDashboardWrapIfAdditionalTemplate from "../components/templates/AdminDashboardWrapIfAdditionalTemplate"

import { uiSchema } from "../utils/uiSchema.ts"

export const SingleData = ({ schema, formData, onChange, propertyKey, onSubmit }) => {
  const formRef = useRef(null)
  const [isError, setIsError] = useState(false)

  const params = useParams()
  const { id } = params

  let section = sections.find(section => section.id === id)

  const schemaSelection = { ...schema }
  if (schemaSelection.properties && propertyKey) {
    if (schemaSelection.properties[id]) {
      schemaSelection.properties = {
        [id]: schemaSelection.properties[id]
      }
    }
  }

  if (!id) {
    section = {
      name: "Home"
    }
  }

  function onError(type) {
    console.log(type)
    setIsError(true)
  }

  return (
    <>
      <Paper className={"paper"}>
        <div className={"contentWrapper"}>
          {true && <Typography variant="h3">{section.title}</Typography>}
          <Form
            key={id}
            ref={formRef}
            schema={schemaSelection}
            uiSchema={uiSchema}
            validator={validator}
            onChange={onChange}
            formData={formData}
            onSubmit={onSubmit}
            onError={onError}
            templates={{
              FieldTemplate: AdminDashboardFieldTemplate,
              WrapIfAdditionalTemplate: AdminDashboardWrapIfAdditionalTemplate
            }}
          >
            <></>
            {onSubmit && (
              <Button variant="contained" type="submit">
                SUBMIT
              </Button>
            )}
          </Form>
        </div>
        {isError && (
          <SingleData
            schema={schema}
            formData={formData}
            onChange={onChange}
            onSubmit={() => {
              setIsError(false)
              onSubmit()
            }}
            propertyKey={false}
          />
        )}
      </Paper>
    </>
  )
}

SingleData.propTypes = {
  schema: PropTypes.object.isRequired,
  formData: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
  propertyKey: PropTypes.bool,
  onSubmit: PropTypes.func
}
