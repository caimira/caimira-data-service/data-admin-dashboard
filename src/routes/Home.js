import React from "react"

import { Button, Paper, Typography, Grid } from "@mui/material"

import sections from "../data/sections"

import { useNavigate } from "react-router-dom"

export const HomePage = () => {
  const navigate = useNavigate()

  return (
    <Paper className={"paper"}>
      <div className={"contentWrapper"}>
        {true && <Typography variant="h3">ARIA Data Service / Parameter Database</Typography>}
        <Typography>
          This dashboard displays parameters that are part of the database but are not directly entered by users in the
          calculator.
        </Typography>
        <br />
        <Grid container spacing={2}>
          {sections.map((section, index) => (
            <Grid item key={index} xs={12} sm={12} md={6} lg={6}>
              <Button
                onClick={e => {
                  e.preventDefault()
                  navigate(section.id)
                }}
                fullWidth
                variant="contained"
                color="primary"
                size="large"
                href={section.id}
              >
                <Typography variant="button">{section.title}</Typography>
              </Button>
            </Grid>
          ))}
        </Grid>
      </div>
    </Paper>
  )
}
