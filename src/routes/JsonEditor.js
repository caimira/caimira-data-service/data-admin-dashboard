import React from "react"

import PropTypes from "prop-types"

import { Paper, Typography } from "@mui/material"

import { Editor } from "@monaco-editor/react"

export const JsonEditor = ({ setData, data }) => {
  const handleEditorChange = value => {
    try {
      const parsedJson = JSON.parse(value)
      setData(parsedJson)
    } catch (error) {
      console.error("Invalid JSON:", error)
    }
  }

  return (
    <>
      <Paper className={"paper"}>
        <div className={"contentWrapper"}>
          {true && <Typography variant="h3">JSON Editor</Typography>}
          <br />
          <>
            <Editor
              height="90vh"
              defaultLanguage="json"
              defaultValue={JSON.stringify(data, null, 2)}
              onChange={handleEditorChange}
              options={{
                automaticLayout: true,
                formatOnType: true,
                formatOnPaste: true
              }}
            />
          </>
        </div>
      </Paper>
    </>
  )
}

JsonEditor.propTypes = {
  data: PropTypes.object.isRequired,
  setData: PropTypes.func.isRequired
}
