import React from "react"
import PropTypes from "prop-types"
import { Button, Paper, Typography } from "@mui/material"
import { useState } from "react"
import { useSnackbar } from "notistack"
import { SingleData } from "./SingleData"
import Ajv from "ajv"

const ajv = new Ajv({ allErrors: true, verbose: true })

export const JsonExport = ({ schema, formData, onChange, onSubmit }) => {
  const [invalid, setInvalid] = useState(false)
  const { enqueueSnackbar } = useSnackbar()

  return (
    <>
      <Paper className={"paper"}>
        <div className={"contentWrapper"}>
          {true && <Typography variant="h3">JSON Export</Typography>}
          {!invalid && (
            <>
              <pre className="admin-dashboard-pre">{JSON.stringify(formData, null, 2)}</pre>
              <Button
                variant="contained"
                type="button"
                onClick={() => {
                  try {
                    if (ajv.validate(schema, formData)) {
                      onSubmit()
                    } else {
                      enqueueSnackbar(`Schema and Data don't match:\n${ajv.errors}`, { variant: "error" })
                      setInvalid(true)
                      return
                    }
                  } catch (error) {
                    enqueueSnackbar(`Problem with data structure:\n${error.message}`, { variant: "error" })
                    setInvalid(true)
                    return
                  }
                }}
              >
                SUBMIT
              </Button>
            </>
          )}
          {invalid && (
            <SingleData
              schema={schema}
              formData={formData}
              onChange={onChange}
              onSubmit={onSubmit}
              propertyKey={false}
            />
          )}
        </div>
      </Paper>
    </>
  )
}

JsonExport.propTypes = {
  schema: PropTypes.object.isRequired,
  formData: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired
}
