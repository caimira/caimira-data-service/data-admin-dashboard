import React from "react"
import PropTypes from "prop-types"
import { useState, useEffect } from "react"
import { Route, Routes } from "react-router-dom"
import CssBaseline from "@mui/material/CssBaseline"
import Box from "@mui/material/Box"
import { useSnackbar } from "notistack"
import Navigator from "../components/Navigator"
import Header from "../components/Header"
import { Footer } from "../components/Footer"

import { SingleData } from "./SingleData"
import { JsonExport } from "./JsonExport"
import { JsonEditor } from "./JsonEditor"
import { HistoryPage } from "./History"
import { SchemaPage } from "./Schema"
import { HomePage } from "./Home"

const drawerWidth = "20%"

const Authenticated = ({ api }) => {
  const [schema, setSchema] = useState({ json_schema: {}, version: undefined })
  const [data, setData] = useState({ json_data: {}, version: undefined }) // eslint-disable-line no-unused-vars
  const [formData, setFormData] = useState({ json_data: {}, version: undefined })
  const { enqueueSnackbar } = useSnackbar()

  const getData = () => {
    api.getData({
      onFailure: () => enqueueSnackbar(`missing_json_data:\nCould not retrieve the data json`, { variant: "error" }),
      onSuccess: response => {
        if (data.version !== response.version) {
          setData(response)
          setFormData(response.data)
        }
      }
    })
  }

  useEffect(() => {
    api.getDataSchema({
      onFailure: () => enqueueSnackbar(`missing_schema:\nCould not retrieve the schema`, { variant: "error" }),
      onSuccess: response => {
        if (schema.version !== response.version) {
          console.log("schema", response)
          setSchema(response)
        }
      }
    })
    getData()
  }, [])

  const onSuccessfulUpdate = () => {
    enqueueSnackbar(`update_successful:\nUpdated json schema`, { variant: "success" })
    getData()
  }

  const onChange = e => {
    setFormData({ ...formData, ...e.formData })
  }

  const onSubmit = () => {
    api.updateData({
      json_data: formData,
      reason: "User update using raw editor",
      onSuccess: onSuccessfulUpdate,
      onFailure: error => {
        enqueueSnackbar(`Invalid Schema:\n${error}`, { variant: "error" })
      }
    })
  }

  return (
    <Box sx={{ display: "flex", minHeight: "100vh" }}>
      <CssBaseline />
      <Box component="nav" sx={{ width: { sm: drawerWidth }, flexShrink: { sm: 0 } }}>
        <Navigator PaperProps={{ style: { width: drawerWidth } }} sx={{ display: { sm: "block", xs: "none" } }} />
      </Box>
      <Box sx={{ flex: 1, display: "flex", flexDirection: "column" }}>
        <Header />
        <Box component="main" sx={{ flex: 1, py: 6, px: 4, bgcolor: "#eaeff1" }}>
          <Routes>
            <Route path="json-editor" element={<JsonEditor setData={setData} data={formData} />} />
            <Route
              path="json"
              element={
                <JsonExport schema={schema.json_schema} formData={formData} onChange={onChange} onSubmit={onSubmit} />
              }
            />
            <Route path="history" element={<HistoryPage api={api} />} />
            <Route path="schema" element={<SchemaPage api={api} />} />
            <Route path="/" element={<HomePage />} />
            <Route
              path=":id?"
              element={
                <SingleData schema={schema.json_schema} formData={formData} onChange={onChange} propertyKey={true} />
              }
            />
          </Routes>
        </Box>
        <Footer />
      </Box>
    </Box>
  )
}

Authenticated.propTypes = {
  api: PropTypes.object.isRequired
}

export default Authenticated
