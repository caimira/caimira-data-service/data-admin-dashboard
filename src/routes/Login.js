import React from "react"
import { Box, Button } from "@mui/material"
import { useMsal } from "@azure/msal-react"
import { loginRequest } from "../authConfig"

function LoginPage() {
  const { instance } = useMsal()
  const handleSubmit = event => {
    console.log(event)
    instance.loginRedirect(loginRequest).catch(e => {
      console.log(e)
    })
  }
  return (
    <div className="login">
      <h3>Caimira Data Dashboard</h3>
      <Box
        component="form"
        sx={{
          "& > :not(style)": { m: 1, width: "25ch" }
        }}
        autoComplete="off"
      >
        <Button className="submit" type="button" variant="contained" onClick={handleSubmit}>
          Login
        </Button>
      </Box>
    </div>
  )
}

export default LoginPage
