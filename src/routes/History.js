import React, { useEffect, useState } from "react"
import PropTypes from "prop-types"
import DifferenceViewer from "../components/DifferenceViewer"

import { Button, Switch, FormGroup, FormControlLabel, Paper, Typography } from "@mui/material"

import Table from "@mui/material/Table"
import TableBody from "@mui/material/TableBody"
import TableCell from "@mui/material/TableCell"
import TableContainer from "@mui/material/TableContainer"
import TableHead from "@mui/material/TableHead"
import TableRow from "@mui/material/TableRow"

export const HistoryPage = ({ api }) => {
  const [history, setHistory] = useState({})
  const [pageNumber, setPageNumber] = useState(0)
  const [selectedRow, setSelectedRow] = useState(null)
  const [collapse, setCollapse] = useState(true)

  useEffect(() => {
    api.getDataHistory({
      page_number: pageNumber,
      page_size: 10,
      onFailure: error => console.log(error),
      onSuccess: response => {
        setHistory(response)
      }
    })
  }, [pageNumber])

  return (
    <Paper className={"paper"}>
      <div className={"contentWrapper"}>
        {true && <Typography variant="h3">History</Typography>}
        <div className="history_list">
          <FormGroup>
            <FormControlLabel
              control={
                <Switch
                  defaultChecked
                  onChange={() => {
                    setCollapse(!collapse)
                  }}
                />
              }
              label="Collapse all"
            />
          </FormGroup>
          <TableContainer component={Paper}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Version</TableCell>
                  <TableCell>Schema Version</TableCell>
                  <TableCell>Reason</TableCell>
                </TableRow>
              </TableHead>
              {history?.history?.map((item, index) => (
                <TableBody key={index}>
                  <TableRow
                    onClick={() => {
                      if (selectedRow != index) {
                        setSelectedRow(index)
                      } else {
                        setSelectedRow(null)
                      }
                    }}
                  >
                    <TableCell>{item.version}</TableCell>
                    <TableCell>{item.schema_version}</TableCell>
                    <TableCell>{item.reason}</TableCell>
                  </TableRow>
                  {(selectedRow === index || !collapse) && (
                    <TableRow className="diff">
                      <TableCell colSpan={3}>
                        <DifferenceViewer key={index} className="difference_viewer" differences={item} />
                      </TableCell>
                    </TableRow>
                  )}
                </TableBody>
              ))}
            </Table>
          </TableContainer>
          {pageNumber > 0 && (
            <Button className="back" type="button" variant="contained" onClick={() => setPageNumber(pageNumber - 1)}>
              Back
            </Button>
          )}
          {pageNumber + 1}
          {pageNumber < history.pages - 1 && (
            <Button className="next" type="button" variant="contained" onClick={() => setPageNumber(pageNumber + 1)}>
              Next
            </Button>
          )}
        </div>
      </div>
    </Paper>
  )
}

HistoryPage.propTypes = {
  api: PropTypes.object.isRequired
}
