import React, { useEffect, useState } from "react"
import PropTypes from "prop-types"
// import classes from "./Home.module.scss"
import { Button, Paper, Typography } from "@mui/material"
import { useSnackbar } from "notistack"
import { Navigate } from "react-router-dom"
import { useMsal } from "@azure/msal-react"
// import "brace/mode/json"
// import "brace/theme/github"

import Ajv from "ajv"

const ajv = new Ajv({ allErrors: true, verbose: true })

export const SchemaPage = ({ api }) => {
  const [json_schema, setJsonSchema] = useState({ json_schema: {} })
  const [schemaArea, setSchemaArea] = useState("")
  const [dataArea, setDataArea] = useState("")
  const { instance } = useMsal()
  const { enqueueSnackbar } = useSnackbar()

  function getData() {
    api.getData({
      onFailure: () => enqueueSnackbar(`missing_json_data:\nCould not retrieve the data json`, { variant: "error" }),
      onSuccess: response => {
        setDataArea(JSON.stringify(response.json_data, null, 4))
      }
    })
  }

  function onSuccessfulUpdate() {
    enqueueSnackbar(`update_successful:\nUpdated json schema`, { variant: "success" })
    getData()
  }

  useEffect(() => {
    getData()
    api.getDataSchema({
      onFailure: () => enqueueSnackbar(`missing_schema:\nCould not retrieve the schema`, { variant: "error" }),
      onSuccess: response => {
        // if (json_schema.version !== response.version) {
        setJsonSchema(response)
        setSchemaArea(JSON.stringify(response.json_schema, null, 4))
        // }
      }
    })
  }, [])

  const accounts = instance.getAllAccounts()

  if (accounts.length > 0) {
    const roles = accounts[0]?.idTokenClaims?.roles
    if (!(roles.includes("admin") || roles.includes("expert"))) {
      return <Navigate to={{ pathname: "/" }} />
    }
  }

  return (
    <Paper className={"paper"}>
      <div className={"contentWrapper"}>
        {true && <Typography variant="h3">Schema / Editor</Typography>}
        <div className="data">
          <div>
            <Typography>Schema Version: {json_schema.version}</Typography>
          </div>
          <>
            {false && <h1>Schema:</h1>}
            <Typography variant="h6">Schema</Typography>
            <textarea value={schemaArea || ""} onChange={e => setSchemaArea(e.target.value)}></textarea>
            {false && <h1>Updated Data:</h1>}
            <Typography variant="h6">Updated Data</Typography>
            <textarea value={dataArea || ""} onChange={e => setDataArea(e.target.value)}></textarea>
            <Button
              variant="contained"
              onClick={() => {
                try {
                  const newSchema = JSON.parse(schemaArea)
                  const newData = JSON.parse(dataArea)
                  if (ajv.validate(newSchema, newData)) {
                    api.updateDataSchema({
                      json_schema: newSchema,
                      json_data: newData,
                      reason: "User update using raw text editor",
                      onSuccess: onSuccessfulUpdate,
                      onFailure: error => {
                        enqueueSnackbar(`Error updating:\n${error}`, { variant: "error" })
                      }
                    })
                  } else {
                    enqueueSnackbar(`Schema and Data don't match:\n${ajv.errors}`, { variant: "error" })
                    return
                  }
                } catch (error) {
                  enqueueSnackbar(`Problem with data structure:\n${error.message}`, { variant: "error" })
                  console.log(error)
                  return
                }
              }}
            >
              SUBMIT
            </Button>
          </>
        </div>
      </div>
    </Paper>
  )
}

SchemaPage.propTypes = {
  api: PropTypes.object.isRequired
}
