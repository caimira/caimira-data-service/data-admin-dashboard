FROM node:16-alpine as build
ARG FRONTEND_ENV_FILE
ARG REACT_APP_VERSION

RUN test -n "$FRONTEND_ENV_FILE" || (echo "FRONTEND_ENV_FILE not set" && false)

RUN npm install -g npm@8.19.2

WORKDIR /app
COPY package.json package-lock.json ./
COPY $FRONTEND_ENV_FILE ./.env
COPY ./src ./src
COPY ./public ./public

RUN if [ -z "$REACT_APP_VERSION" ]; then \
    echo "REACT_APP_VERSION not set, reverting to package.json version"; \
    echo -e "\nREACT_APP_VERSION = $(node -p "require('./package.json').version")" >> ./.env; \
  else \
    echo -e "\nREACT_APP_VERSION = $REACT_APP_VERSION" >> ./.env; \
  fi

RUN npm install --legacy-peer-deps

Run npm run build

RUN npm install -g serve

EXPOSE 3000
CMD ["serve", "-s", "build"]