# CAiMIRA Data Service Administrative Dashboard


## Installation
```bash
npm install
```

## Running the dashboard
```bash
npm start
```

## Running in a container using podman
```bash
# Start the VM
podman machine start
# Build the image
podman build -t dashboard:latest .
# Run the image
podman run --name dashboard -p 3000:3000 dashboard:latest
```

If there is need to stop and re-run the image:
```bash
# Stop the container
podman stop dashboard
# Remove the container
podman rm dashboard
# Run the image
podman run --name dashboard -p 3000:3000 dashboard:latest
```

If you need to rebuild the image after modification of code or the `Dockerfile`:
```bash
# Stop the container
podman stop dashboard
# Remove the container
podman rm dashboard
# Remove the image
podman image rm dashboard
# Build the new image
podman build -t dashboard:latest .
# Run the new image
podman run --name dashboard -p 3000:3000 dashboard:latest
```

To open a shell into the container, with the container running:

```
podman exec -it dashboard /bin/bash
```